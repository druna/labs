import math
import matplotlib.pyplot as plt

while True:
    # Блок для выбора функции и ввода переменных
    try:
        number = int(input('Выберите номер функции(1 - G, 2 - F, 3 - Y):'))
        if number in [1, 2, 3]:
            x_max, x_min = float(input('Введите переменную x_max(число):')), float(
                input('Введите переменную x_min(число):'))
            a = float(input('Введите переменную a(число):'))
            step_lot = int(input('Введите количество шагов(целое число): '))
        else:
            print('Номер функции находится в диапазоне от 1 до 3')
    except ValueError:
        print('Вводить нужно то, что просят')
    if step_lot == 0:
        print('Шаг равен нулю')
    elif x_min > x_max:
        print('x_min не может быть больше x_max')
    else:
        x_lst = []  # Лист для значений x
        y_lst = []  # Лист для значений f(x)
        step_x = (x_max-x_min)/step_lot

        # Заполнение листов
        if number == 1:
            for i in range(step_lot+1):
                x = x_min + step_x * i
                x_lst.append(x)
                try:
                    G = 9 * (7 * a ** 2 + 39 * a * x + 20 * x ** 2) / (9 * a ** 2 + 59 * a * x + 30 * x ** 2)
                    y_lst.append(G)
                except ZeroDivisionError:
                    y_lst.append(None)
        elif number == 2:
            for i in range(step_lot+1):
                x = x_min + step_x * i
                x_lst.append(x)
                try:
                    F = math.atanh(10 * a ** 2 - 43 * a * x + 28 * x ** 2)
                    y_lst.append(F)
                except ValueError:
                    y_lst.append(None)
        elif number == 3:
            for i in range(step_lot+1):
                x = x_min + step_x * i
                x_lst.append(x)
                try:
                    Y = math.log(-10 * a ** 2 - 27 * a * x + 28 * x ** 2 + 1) / (math.log(10))
                    y_lst.append(Y)
                except ValueError:
                    y_lst.append(None)

        # Построение графика
        plt.title("График функции")
        plt.xlabel("x")
        plt.ylabel("f(x)")
        plt.grid(which='major', c='black', linewidth=1)
        plt.plot(x_lst, y_lst, c='red')
        plt.show()

    # Блок выхода
    if input('Для продолжения программы введите y: ') != 'y':
        print('Программа завершилась')
        break
    else:
        print('Программа продолжается')
